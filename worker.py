from pika import BasicProperties
from inference_model import MNISTInferenceModel
import pickle
from bridge import rabbitmq_client, redis_client


def callback(ch, method, properties: BasicProperties, body):
    image_data = pickle.loads(body)
    result = inference_model.infer(image_data)
    print(f"Result: {result}")

    redis_client.set(properties.headers["inference_id"], result)

    ch.basic_ack(delivery_tag=method.delivery_tag)


inference_model = MNISTInferenceModel()

rabbitmq_client.basic_qos(prefetch_count=1)
rabbitmq_client.basic_consume(queue='mnist_inference_queue', on_message_callback=callback)

rabbitmq_client.start_consuming()
